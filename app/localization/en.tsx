export default {
  home: {
    title: 'Home',
  },
  movies: {
    title: 'Movies',
    now_showing: 'Now showing',
    coming_soon: 'Coming soon',
  },
  ok: 'Ok',
  no: 'No',
};
