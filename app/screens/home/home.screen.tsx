import {useNavigation} from '@react-navigation/core';
import React, {useContext, useEffect} from 'react';
import {Text, View} from 'react-native';
import {MFPContext} from '../../contexts/mfp.context';
import {obtainAccessToken} from '../../services/mfp.service';
import {HomeStyles} from './home.styles';

const HomeScreen = () => {
  const [token, setToken] = useContext(MFPContext);
  const navigation = useNavigation();

  useEffect(() => {
    obtainAccessToken().then(token => {
      // console.log('obtainAccessToken', token);
      setToken(token);
      navigation.navigate('Movies');
    });
  }, []);

  return (
    <View style={HomeStyles.container}>
      <Text>Connection to the server {JSON.stringify(token)} </Text>
    </View>
  );
};
export default HomeScreen;
