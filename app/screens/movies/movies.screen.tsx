import {useNavigation} from '@react-navigation/core';
import React, {useContext, useEffect, useState} from 'react';
import {ActivityIndicator, TouchableOpacity, View} from 'react-native';
import {ButtonGroup, Header} from 'react-native-elements';
import {Image} from 'react-native-elements/dist/image/Image';
import {ScrollView} from 'react-native-gesture-handler';
import {MFPContext} from '../../contexts/mfp.context';
import {strings} from '../../localization/localization.config';
import {wLResourceRequest} from '../../services/mfp.service';
import {MoviesStyles} from './movies.styles';

const MoviesScreen = () => {
  const [token, setToken] = useContext(MFPContext);

  const [updateIndex, setUpdateIndex] = useState(0);

  const [loading, setLoading] = useState(false);
  const [films, setFilms] = useState({});

  const navigation = useNavigation();

  function filmsNowShowing() {
    setLoading(true);
    wLResourceRequest('filmsNowShowing').then(films => {
      setFilms(films);
      setLoading(false);
    });
  }

  function filmsComingSoon() {
    setLoading(true);
    wLResourceRequest('filmsComingSoon').then(films => {
      setFilms(films);
      setLoading(false);
    });
  }

  useEffect(() => {
    console.log(token);
    filmsNowShowing();
  }, []);

  const DrawButtonGroup = () => {
    return (
      <ButtonGroup
        onPress={value => {
          setUpdateIndex(value);
          if (value == 0) {
            filmsNowShowing();
          } else {
            filmsComingSoon();
          }
        }}
        selectedIndex={updateIndex}
        buttons={[strings.movies.now_showing, strings.movies.coming_soon]}
        containerStyle={MoviesStyles.button_group}
        disabled={loading}
      />
    );
  };

  const DrawPosterGrid = row => {
    return (
      <TouchableOpacity
        style={MoviesStyles.grid}
        onPress={() => navigation.navigate('Movies')}>
        <Image
          style={MoviesStyles.poster}
          source={{
            uri: row.images.poster['1'].medium.film_image,
          }}
        />
      </TouchableOpacity>
    );
  };

  return (
    <View style={MoviesStyles.root}>
      <Header
        leftComponent={loading ? <ActivityIndicator color="#0000ff" /> : <></>}
        centerComponent={{text: strings.movies.title}}
      />
      <View>{DrawButtonGroup()}</View>

      {films && films?.films && films?.films?.length ? (
        <ScrollView>
          <View style={MoviesStyles.gridContainer}>
            {films.films
              .filter(r => r?.images?.poster['1'])
              .map(row => DrawPosterGrid(row))}
          </View>
        </ScrollView>
      ) : (
        <></>
      )}
    </View>
  );
};
export default MoviesScreen;
