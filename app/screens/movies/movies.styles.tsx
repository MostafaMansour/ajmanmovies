import {StyleSheet} from 'react-native';

// styles
export const MoviesStyles = StyleSheet.create({
  root: {
    flex: 1,
  },
  grid: {
    borderWidth: 2,
    borderColor: 'transparent',
    width: '50%',
    height: 300,
    boxSizing: 'border-box',
    // margin: 10,
  },
  gridContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 10,
  },
  poster: {
    width: '100%',
    height: '100%',
  },
  button_group: {
    height: 40,
  },
});
