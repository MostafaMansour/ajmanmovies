import {
  WLAuthorizationManager,
  WLResourceRequest,
} from 'react-native-ibm-mobilefirst';

let __token: string;

const setToken = (token: string) => {
  __token = token;
  // return SecureStore.setItemAsync('secure_token', token);
};

export const getToken = () => {
  return __token;
};

export const obtainAccessToken = async () =>
  WLAuthorizationManager.obtainAccessToken('').then(
    (token: string) => {
      console.log('token', token);
      setToken(token);
      return Promise.resolve(token);
    },
    (error: any) => {
      console.error(error);
      console.log('Failed to connect to MobileFirst Server');
      return Promise.reject(error);
    },
  );

export const wLResourceRequest = async (endpoint: string) => {
  return new Promise((resolve, reject) => {
    console.log('-->  pingMFP(): Success ', getToken());
    let resourceRequest = new WLResourceRequest(
      `/adapters/AjmanMoviesAdapter/${endpoint}`,
      WLResourceRequest.GET,
    );
    resourceRequest.setQueryParameters({name: 'world'});
    resourceRequest.send().then(
      (response: {responseJSON: object}) => {
        // Will display "Hello world" in an alert dialog.
        console.log('Success: ' + response.responseJSON);
        return resolve(response.responseJSON);
      },
      (error: any) => {
        console.error(error);
        console.log('Failure: Resource Request');
        return reject(error);
      },
    );
  });
};
