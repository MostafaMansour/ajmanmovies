export interface filmsNowShowing {
  images: {
    still: {
      '1': {
        image_orientation: string;
        medium: {
          film_image: string;
          width: number;
          height: number;
        };
      };
    };
    poster: {
      '1': {
        image_orientation: string;
        medium: {
          film_image: string;
          width: number;
          height: number;
        };
        region: string;
      };
    };
  };
  imdb_id: number;
  age_rating: {
    age_advisory: string;
    rating: string;
    age_rating_image: string;
  }[];
  release_dates: {
    notes: string;
    release_date: string;
  }[];
  synopsis_long: string;
  film_name: string;
  other_titles: string;
  film_id: number;
  imdb_title_id: string;
  film_trailer: string;
}
