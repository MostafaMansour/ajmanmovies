import React, {useState} from 'react';

const MFPContext = React.createContext([{}, () => {}]);

const MFPProvider = props => {
  const [state, setState] = useState({});
  return (
    <MFPContext.Provider value={[state, setState]}>
      {props.children}
    </MFPContext.Provider>
  );
};

export {MFPContext, MFPProvider};
