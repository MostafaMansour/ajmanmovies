/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {strings} from './app/localization/localization.config';
import HomeScreen from './app/screens/home/home.screen';
import {MFPProvider} from './app/contexts/mfp.context';
import MoviesScreen from './app/screens/movies/movies.screen';

const Stack = createStackNavigator();

const App = () => {
  return (
    <MFPProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Home">
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{title: strings.home.title}}
          />
          <Stack.Screen
            name="Movies"
            component={MoviesScreen}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </MFPProvider>
  );
};
export default App;
